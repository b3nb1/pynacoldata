import asyncio
import psycopg2

from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN
from event import live_data_event_pb2 as liveDataEvent
from random import *

async def db_storage(msg):
    con = psycopg2.connect(database="postgres", user="postgres", password="secret", host="localhost", port="5432")
    cur = con.cursor()
    #Uncomment this for the very first call of this function to create db schema.
    # cur.execute('''CREATE TABLE LIVEDATA
    #       (ID INT PRIMARY KEY     NOT NULL,
    #       COMPOUND           TEXT    NOT NULL,
    #       DATA            BYTEA     NOT NULL,
    #       SENSOR_COMPOUND        CHAR(50),
    #       TIMESTAMP        CHAR(50));''')
    # print("Table created successfully")

    admission = randint(1, 1000)
    cur.execute(
        "INSERT INTO LIVEDATA (admission,compound, data, sensor_compound, timestamp) "
        "VALUES (%s, %s, %s, %s, %s)",
        (admission, msg.proto.subject, msg.data, 'sensor_compound_name', msg.timestamp)
    )
    print("added sequence {} to database".format(msg.sequence))
    con.commit()
    con.close()


async def run(loop):
    nc = NATS()
    await nc.connect(io_loop=loop)
    sc = STAN()
    await sc.connect("test-cluster", "listener-3F45", nats=nc)
    data = liveDataEvent.LiveDataEvent()  # for now its useless, for later (pb3) maybe helpful

    async def cb(msg):
        await db_storage(msg)

    subject = "TestCompount"
    await sc.subscribe(subject, durable_name="listener", cb=cb)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.run_forever()
