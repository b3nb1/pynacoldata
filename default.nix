with import <nixpkgs> {};
with python37Packages;
stdenv.mkDerivation {
  name = "datalogger";
  src = null;
  buildInputs = [
    python37Full
    python37Packages.virtualenv
    python37Packages.pip
    python37Packages.setuptools
    python.pkgs.venvShellHook
  ];
  shellHook = ''
    virtualenv --no-setuptools venv
    export PATH=$PWD/venv/bin:$PATH
    export PYTHONPATH=venv/lib/python3.7/site-packages/:$PYTHONPATH
  '';
  postShellHook = ''
    ln -sf PYTHONPATH/* ${virtualenv}/lib/python3.7/site-packages
  '';
}
