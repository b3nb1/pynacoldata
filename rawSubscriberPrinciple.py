import asyncio
from nats.aio.client import Client as NATS
from stan.aio.client import Client as STAN


async def run(loop):
    nc = NATS()
    await nc.connect(io_loop=loop)

    sc = STAN()
    await sc.connect("test-cluster", "subscriber_worker", nats=nc)

    total_messages = 0
    future = asyncio.Future(loop=loop)
    async def cb(msg):
        print("Received a message (seq={}): {}".format(msg.seq, msg.data))
        # Receive all stored values in order
        await sc.subscribe("TestCompound", deliver_all_available=True, cb=cb)

    sub = await sc.subscribe("TestCompound", start_at='first', cb=cb)
    await asyncio.wait_for(future, 1, loop=loop)

    # Stop receiving messages
    await sub.unsubscribe()

    # Close NATS Streaming session
    await sc.close()

    # We are using a NATS borrowed connection so we need to close manually.
    await nc.close()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()